const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//Product Schema
//Id add automatically
const productSchema = new Schema({
    title:{
       type:String,
       required:true
    },
    price:{
        type:Number,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    imageUrl:{
        type:String,
        required:true
    },
    userId:{
        type: Schema.Types.ObjectId,
        ref:'User', //On précise de quelle model vient la référence
        required:true
    }
})

module.exports = mongoose.model('Product',productSchema)

