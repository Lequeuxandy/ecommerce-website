const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//User Schema 
//Id add automatically with mangoose
const userSchema = new Schema({
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    //allow to reset password 
    resetToken: String,
    resetTokenExpiration:Date,
    cart:{
        items:[  //[{}]->array of documents [bool,string,number ...]
            {
                productId:{ type: Schema.Types.ObjectId ,ref:'Product', required:true},
                quantity:{type:Number,required:true},
                //We need to save the unit price of a product because if the admin removes a product while  this product is in the user cart, it will be necessary on the one hand to suppress this product and especially to update the total price
                //Since the price is accessible by reference, it will not be accessible once the product has been removed.
                //we need to store the price of each item in the cart of the user to be able to update the total price when a product was removed from the database
                unitprice:{type:Number,required:true}   

            }
        ],
        totalPrice:{
            type:Number,
            required:true
        }   
    }  
});

//hook a method to the schema wich allow to add a product in Cart of the user
//this refer to Shema
userSchema.methods.addToCart = function(product){
      //we search if the product exists in the cart of the user
        const cartProductIndex = this.cart.items.findIndex(cp => {
            return cp.productId.toString() === product._id.toString();
        });

        let totalPrice = 0;
        totalPrice = this.cart.totalPrice + product.price;
        
        let newQuantity = 1;
        const updatedCartItems = [...this.cart.items]
        
        //if you already have the same product in the cart
        if(cartProductIndex >= 0){
            newQuantity = this.cart.items[cartProductIndex].quantity + 1;
            updatedCartItems[cartProductIndex].quantity = newQuantity;
        } else {
            updatedCartItems.push({
                productId:product._id, 
                quantity:newQuantity,
                unitprice:product.price
            })
        }
        
        const updatedCart = {
            items:updatedCartItems,
            totalPrice
        }
      
        this.cart = updatedCart;
        return this.save();
}

//hook a method to the schema wich allow to delete a product from Cart user
userSchema.methods.deleteItemFromCart = function(product){

    
    const indexItemDelete = this.cart.items.findIndex(item=>{
        return  item.productId.toString() == product._id.toString();
    })
    const quantity = this.cart.items[indexItemDelete].quantity;

    this.cart.totalPrice -= (product.price*quantity);

    const updatedCartItems = this.cart.items.filter(item=>{
        return  item.productId.toString() !== product._id.toString();
    });

    this.cart.items = updatedCartItems;
    
    return this.save();
}

//hooks a method to the userSchema wich allow to clear Cart user
userSchema.methods.clearCart = function(){
    this.cart = {items:[],totalPrice:0};
    return this.save();
}

//hooks a method to the userSchema wich allow to update Cart user
userSchema.methods.updateCart = function(updateCartItem){
    let totalPriceUpdate = 0;
    if(updateCartItem.length>0){
        const tabPrice = updateCartItem.map(p=>p.unitprice*p.quantity)
        totalPriceUpdate = tabPrice.reduce((accumulator, currentValue) => accumulator + currentValue);
    }
    this.cart = {items:updateCartItem,totalPrice:totalPriceUpdate};
    console.log('cartUpdate')
    return this.save();
}


//mongo automatically take plurial and lowercase version of 'User'
module.exports = mongoose.model('User',userSchema)



