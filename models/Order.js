const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//Order Shema in mangoose
//Id add automatically with mangoose
const orderSchema = new Schema({
    products:[ {
        product:{ type: Object,required:true },
        quantity:{type:Number,required:true}
    }],
    totalPrice:{
        type:Number,
        required:true
    },
    user:{
        email:{
            type: String,
            required:true
        },
       userId:{
           type:Schema.Types.ObjectId,
           required:true,
           ref:'User'
       }
    }
    
});

 

module.exports = mongoose.model('Order',orderSchema)