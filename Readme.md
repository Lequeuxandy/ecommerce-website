# Ecommerce website - Nodejs

Build modern Ecommerce website full stack, fast and scalable 🥞 (client, server, database)

With the following features:

Global features : 

*  Create User 
*  Display all products

Admin part :

*  Add/Read/Update/delete product 

User part:

*  Get detail of product
*  add product in cart User
*  delete product from cart User
*  Order 
***
##Sources

* Udemy:https://www.udemy.com/nodejs-the-complete-guide/
* Udemy:https://www.udemy.com/nodejs-api-rest/


