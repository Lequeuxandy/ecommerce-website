
const express= require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')


const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');
const uniqid = require('uniqid');

const MONGODB_URI = require('./util/database');
const secretSession = require('./util/session_secret');
const shopController = require('./controllers/shop');
const isAuth = require('./middleware/is-auth');


const app = express();
//❗ MongoDBStore contains data but not method hook to the mangoose model
const store = new MongoDBStore({
    uri:MONGODB_URI,
    collection: 'sessions',
});

const csrfProtection = csrf();


const fileStorage = multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null, 'images');
    },
    filename:(req,file,cb)=>{
        const uniqueName = uniqid.process()+'-'+file.originalname;
        console.log(uniqueName)
        cb(null,uniqueName);
    }
});

const fileFilter = (req, file, cb)=>{
    if(file.mimetype==='image/png' || file.mimetype==='image/jpg' || file.mimetype==='image/jpeg'){
        cb(null,true);
    } else {
        cb(null,false);
    }
};

const errorController = require('./controllers/errors')
const adminRoutes= require('./routes/admin'),
shopRoutes = require('./routes/shop'),
authRoutes = require('./routes/auth');


const path = require('path'),
rootDir = require('./util/path');

const User = require('./models/User')

 //👓EJS built-in 👓//
app.set('view engine','ejs');
app.set('views','views'); 


app.use(bodyParser.urlencoded({extended:false})); 

app.use(multer({storage:fileStorage,fileFilter:fileFilter}).single('image')); //image is called image because in view name is equal to image

app.use(express.static(path.join(__dirname,'public')));  //serve a static file to the client that allows access to read
app.use('/images',express.static(path.join(__dirname,'images')));  //serve a static file to the client that allows access to read


//Session initialization 
//middleware automaticaly set cookies 
app.use(session({
    secret:secretSession,
    resave:false,
    saveUninitialized:false,
    store: store
}));

app.use(flash());

app.use((req,res,next)=>{
    if(!req.session.user){
        return next();
    }
    User.findById(req.session.user._id)
    .then(user=>{
        //if user does not exist 
        if(!user){
            return next();
        }
        req.user = user;
       next();
    })
    //👿 Catch only technical errors but not if the user is not find in db !!
    .catch(err =>{  
        throw new Error(err)
    }); 
});

app.use((req, res, next) => {
    //local variable that are passed into the views
    res.locals.isAuthenticated = req.session.isLoggedin;
    next();
})

//We created route before initialization csrfProtection !
app.post('/create-order', isAuth, shopController.postOrder);

//Order important because package crsf will use session
app.use(csrfProtection)
app.use((req, res, next) => {
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.use('/admin',adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.get('/500',errorController.getError500);

app.use(errorController.getError404);

// ⭐ express error handling middleware 4 arguments ! middleware execute when you passed a error to the next function (next(error))
app.use((error, req, res, next)=>{
    console.log(error)
    //res.status(error.httpStatusCode).render(...);
    res.redirect('/500');
})

 
/*📚 BDD connect 📚*/ 
mongoose.connect(MONGODB_URI)
        .then(result=>{
            app.listen(3000);
        })
        .catch(err=>console.log(err))

