const User = require('../models/User');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer'),
sendgridTransport = require('nodemailer-sendgrid-transport');
const api_key = require('../util/nodemailer_key');
const { validationResult } = require('express-validator/check')

const crypto = require('crypto')

const transporter = nodemailer.createTransport(sendgridTransport({
  auth:{
    api_key:api_key.trim()
  }
}));


exports.getLogin = (req, res, next ) => {
    //  const isLoggedin = req
    //     .get('Cookie')
    //     .trim()
    //     .split('=')[1];
    let message = req.flash('error');
  if(message.length > 0){
    message = message[0];
  } else {
    message = null;
  }
    res.render('auth/login',
              {
                path:'/login' ,
                siteTitle:'Login',
                errorMessage:message,  
                oldInput:{ 
                  email:'',
                  password:'' 
                 },
                 validationErrors: []
              }
      )
  };



exports.postLogin = (req, res, next ) => {
    // //hook new property to the req
    // //data is lost after the request
    // req.isLoggedin = true;  
   const email = req.body.email;
   const password = req.body.password;

   const errors = validationResult(req);
   if(!errors.isEmpty()){
      return res.status(422)
              .render('auth/login',
                {
                  path:'/login' ,
                  siteTitle:'Login',
                  errorMessage:errors.array()[0].msg,
                  oldInput:{ 
                    email,
                    password 
                   },
                   validationErrors: errors.array()
                }
              );
   }
    User.findOne({email})
    .then(user=>{
      if(!user){
          return res.status(422)
          .render('auth/login',
            {
              path:'/login' ,
              siteTitle:'Login',
              errorMessage:'Invalid password or email',
              oldInput:{ 
                email,
                password 
               },
               validationErrors:[]
            }
          );
      }   
        bcrypt.compare(password, user.password)  
              .then(doMatch=>{
                if(doMatch){
                  req.session.isLoggedin =true;
                  req.session.user = user;  //user is a full mongoose model | add a new var for the req.User 
                  //🖐allow to be sure your session was created before redirect!!
                return req.session.save(err=>{
                    console.log(err);
                    res.redirect('/');
                  }); 
                }
                return res.status(422)
                  .render('auth/login',
                    {
                      path:'/login' ,
                      siteTitle:'Login',
                      errorMessage:'Invalid password or email',
                      oldInput:{ 
                        email,
                        password 
                      },
                      validationErrors:[]
                    }
                );
              })
              .catch(err=>console.log(err))
      
    })
    .catch(err=>{
      const error = new Error(err)
      error.httpStatusCode = 500;
      return next(error);
    })
    
};

exports.postLogout = (req, res, next ) => {
  // //hook new property to the req
  // //data is lost after the request
  // req.isLoggedin = true;  
    req.session.destroy(err=>{
      console.log(err);
      res.redirect('/');
    });
  
};

exports.postSignup = (req, res, next ) => {
  const email = req.body.email;
  const password = req.body.password;

  const errors = validationResult(req); //get errors from validation in routes/auth 
  if(!errors.isEmpty()){
    console.log(errors.array())
    return res.status(422)
              .render('auth/signup',
                    {
                      path:'/signup' ,
                      siteTitle:'signup',
                      errorMessage:errors.array()[0].msg,
                      oldInput:{ 
                         email,
                         password ,
                         confirmPassword:req.body.confirmPassword
                        },
                        validationErrors: errors.array()
                    }
    )                    
  }

  bcrypt.hash(password, 12) //second argument allow to set size of the hash password 
              .then(hashedPassword=>{
                    const user = new User({
                        email,
                        password:hashedPassword,
                        cart:{
                          items:[],
                          totalPrice:0
                        }
                      })
                      return user.save();       
              })
              .then(result=>{
                  res.redirect('/login')
              // ⛔ the followings instruction (email) are too limited for large scale app ⛔
                  return transporter.sendMail({
                      to:email.trim(),
                      from:'shop@node-complete.fr',
                      subject:'Signup succeeded!',
                      html:'<h1>You successfully signed up!</h1>'
                    })
            
                })
                .catch(err=>{
                  const error = new Error(err)
                  error.httpStatusCode = 500;
                  return next(error);
                })
};


exports.getSignup = (req, res, next ) => {
  let message = req.flash('error');
  if(message.length > 0){
    message = message[0];
  } else {
    message = null;
  }
  res.render('auth/signup',
            {
              path:'/signup' ,
              siteTitle:'signup',
              errorMessage:message,
              oldInput:{ 
                email:'',
                password:'' ,
                confirmPassword:'',
               },
               validationErrors:[]
            }
    )
};

exports.getReset = (req, res ,next)=>{
  let message = req.flash('error');
  if(message.length > 0){
    message = message[0];
  } else {
    message = null;
  }
  res.render('auth/reset',
  {
    path:'/reset' ,
    siteTitle:'Reset password',
    errorMessage:message
  }
)
}

exports.postReset = (req, res , next)=>{
  crypto.randomBytes(32,(err,buffer)=>{
    if(err){
      console.log(err);
      return res.redirect('/reset');
    }
    const token = buffer.toString('hex')
    User.findOne({email: req.body.email})
        .then(user=>{
          if(!user){
            req.flash('error','No account with that email found');
            return res.redirect('/reset');
          }
          user.resetToken = token;
          user.resetTokenExpiration = Date.now() + 3600000; //3600000 - 1h
          return user.save();
        })
        .then(result=>{
             // ⛔ the followings instruction (email) are too limited for large scale app ⛔
             res.redirect('/');
             transporter.sendMail({
              to:req.body.email,
              from:'shop@node-complete.fr',
              subject:'Password reset',
              html:`
              <p> You requested a password reset</p>
              <p> Click this <a href="http://localhost:3000/reset/${token}">link</a> to set a new password </p>
              `
            })
        })
        .catch(err=>{
          const error = new Error(err)
          error.httpStatusCode = 500;
          return next(error);
        })
  })
}

exports.getNewPassword = (req, res, next) => {
  const token = req.params.token;
  //$gt -> greater than
  User.findOne({resetToken:token, resetTokenExpiration: {$gt:Date.now()}})
        .then(user => {       
              let message = req.flash('error');
              if(message.length > 0){
                message = message[0];
              } else {
                message = null;
              }
              res.render('auth/new-password',
              {
                path:'/new-password' ,
                siteTitle:'New password',
                errorMessage:message,
                userId: user._id.toString(),
                passwordToken:token
              });
        })
        .catch(err=>{
          const error = new Error(err)
          error.httpStatusCode = 500;
          return next(error);
        })
}

exports.postNewPassword = (req, res, next) =>{
  const newPassword = req.body.password;
  const userId = req.body.userId;
  const passwordToken = req.body.passwordToken;
  let resetUser;

  User.findOne({resetToken: passwordToken, resetTokenExpiration:{$gt:Date.now()}, _id:userId})
      .then(user=>{
        resetUser=user;
        return bcrypt.hash(newPassword,12)
      })
      .then(hashedPassword=>{
        resetUser.password = hashedPassword;
        resetUser.resetToken = null;
        resetUser.resetTokenExpiration = undefined;
        return resetUser.save();
      })
      .then(result=>{
        res.redirect('/login');
        transporter.sendMail({
          to:resetUser.email,
          from:'shop@node-complete.fr',
          subject:'Password reset',
          html:`
          <p>Your password has been changed</p>
          `
        })
      })
      .catch(err=>{
        const error = new Error(err)
        error.httpStatusCode = 500;
        return next(error);
      })
      
};

