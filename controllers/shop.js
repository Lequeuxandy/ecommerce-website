const fs = require('fs');
const path = require('path');
const Product = require('../models/Product');
const Order = require('../models/Order');
const PDFdocument = require('pdfkit');
const secretKeyStripe = require('../util/stripeSecretkey');
 // Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
const stripe = require("stripe")(secretKeyStripe);

//Globar var wich allow to specify the number of products to display per page 
const ITEMS_PER_PAGE = 2;


//Render view shop/product-list with product list
exports.getProducts =(req, res, next)=>{
  const page = +req.query.page || 1; //+ convert string to integer
  let totalItems;
  
  // ❗ find() do not return a cursor (compare to find method in mangodb) but list of products
  Product.find().countDocuments()
    .then(numProducts=>{
      totalItems = numProducts;
      return Product.find()
            .skip((page - 1)*ITEMS_PER_PAGE)  //the number of documents to skip in the results set
            .limit(ITEMS_PER_PAGE) //limits the amount of data we fetch
    })
    .then(products=>{
      res.render('shop/product-list',
      {
        prods:products,
        siteTitle:'Products',
        path:'/products',
        currentPage:page,
        hasNextPage:ITEMS_PER_PAGE * page <totalItems,
        hasPreviousPage:page > 1,
        nextPage:page + 1,
        previousPage:page -1,
        lastPage:Math.ceil(totalItems/ITEMS_PER_PAGE)
      }); 
    })
    .catch(err=>{
      const error = new Error(err)
      error.httpStatusCode = 500;
      return next(error);
    })       
  };

//Render view shop/product-detail with product
  exports.getProduct =(req, res, next)=>{
     const productId = req.params.productId;
     //findById() => mongoose method | mongoose convert string in ObjectId automatically
     Product.findById(productId)
            .then(product=>{
              console.log('getProduct',product)
              res.render('shop/product-detail',
              {
                product:product,
                siteTitle:product.title,
                path:'product-detail'  //on reste sur le liens active products          
              })
            })  
            .catch(err=>{
              const error = new Error(err)
              error.httpStatusCode = 500;
              return next(error);
            })   
  };

//Render view shop/index with products list
exports.getIndex = (req, res, next ) => {
  const page = +req.query.page || 1; //+ convert string to integer
  let totalItems;

  Product.find().countDocuments()
    .then(numProducts=>{
      totalItems = numProducts;
      return Product.find()
            .skip((page - 1)*ITEMS_PER_PAGE)  //the number of documents to skip in the results set
            .limit(ITEMS_PER_PAGE) //limits the amount of data we fetch
    })
    .then(products=>{
      res.render('shop/index',
      {
        prods:products,
        siteTitle:'Shop',
        path:'/',
        currentPage:page,
        hasNextPage:ITEMS_PER_PAGE * page <totalItems,
        hasPreviousPage:page > 1,
        nextPage:page + 1,
        previousPage:page -1,
        lastPage:Math.ceil(totalItems/ITEMS_PER_PAGE)
      }); 
    })
    .catch(err=>{
      const error = new Error(err)
      error.httpStatusCode = 500;
      return next(error);
    })    
};
  

//Render view shop/cart with products of the user cart
// ❗ ❗ ❗  it is necessary each time to verify that the products in the cart of the user are present in the database
// if ever the admin comes to remove products in the database
exports.getCart = (req, res, next ) => {
    
  const productIds = req.user.cart.items.map(p=>{
      return p.productId.toString();
    })
     const productIdsCart = [...productIds]
    
     /*We must update cart user according products available in a database (Product schema)*/
     Product.find()
             .select('_id')
             .then(products=>{
                
                //we retrieve the id of the products present in the database
                const productIdsBd = products.map(p=>{
                 return p._id.toString();
                })

             //we get the id products that are in the cart of the user but not in the database! 
             //it will eventually be necessary to remove them
             const productsIdDelete = productIdsCart.filter(id=> !productIdsBd.includes(id.toString()))
            
             //we retrieve the items from the cart present in the cart and in the database
             const updatedItemsCart = req.user.cart.items.filter(p=>!productsIdDelete.includes(p.productId.toString()))
           
             //Xe update user cart 
              return req.user.updateCart(updatedItemsCart)    
              
            })

              //We retrieve the products information available in the cart
              .then(result=>{
                return  req.user
                .populate('cart.items.productId')
                .execPopulate()//Allow to return promise
              })  
              .then(user=>{
                    const products=user.cart.items;
                    const totalPrice = user.cart.totalPrice;
                      res.render('shop/cart',{
                        path:'/cart',
                        siteTitle:'Your Cart',
                        products,
                        totalPrice
                      })
                  })            
                  .catch(err=>{
                    const error = new Error(err)
                    error.httpStatusCode = 500;
                    return next(error);
                  })
};

//Add product in cart user
exports.postCart=(req,res,next)=>{
  const prodId = req.body.productId;
  console.log('postCard')
  Product.findById(prodId)
          .then(product => {
            return req.user.addToCart(product);
          })
          .then(result => {
            res.redirect('/cart')
          })
          .catch(err=>{
            const error = new Error(err)
            error.httpStatusCode = 500;
            return next(error);
          })

}

exports.getCheckout = (req,res,next)=>{
  req.user.populate('cart.items.productId')
          .execPopulate()//Allow to return promise
          .then(user=>{
              const products=user.cart.items;
              const totalPrice = user.cart.totalPrice;
              res.render('shop/checkout',{
                        path:'/checkout',
                        siteTitle:'Checkout',
                        products,
                        totalPrice
                      })
                  })            
                  .catch(err=>{
                    const error = new Error(err)
                    error.httpStatusCode = 500;
                    return next(error);
                  })
}
        
//created an order 
exports.postOrder=(req, res, next)=>{
 
// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
const token = req.body.stripeToken; // Using Express
let totalSum = 0;
  req.user
    .populate('cart.items.productId')
    .execPopulate()  //permet de retourner une promesse
    .then(user=>{
      totalSum = user.cart.totalPrice;
      const products=user.cart.items.map(i=>{
        return {quantity:i.quantity,product:{...i.productId._doc}};  //_doc -> Retrieves an object with all the product data
      });
      console.log(products)
      const order = new Order({
        products,
        totalPrice:req.user.cart.totalPrice,
        user:{
          email: req.user.email,
          userId:req.user
        }
       });    
       return order.save()
    })
    //we must delete all the product in cart of the user 
    .then(result=>{
      const charge = stripe.charges.create({
        amount: totalSum * 100,
        currency: 'eur',
        description: 'Demo order',
        source: token,
        metadata:{order_id:result._id.toString()}
      });
        return req.user.clearCart()   
    })
    .then(()=>{
        console.log('Create Order');
        res.redirect('/orders')
    })
    .catch(err=>{
      const error = new Error(err)
      error.httpStatusCode = 500;
      return next(error);
    })
};


//Render 'shop/orders' views with all orders of the user
exports.getOrders = (req, res, next ) => {
  Order.find({
    'user.userId':req.user._id
  })
  .then(orders=>{
    res.render('shop/orders',
            {
              orders,
              path:'/orders' ,
              siteTitle:'Your Orders'
            }
    )
  })
  .catch(err=>{
    const error = new Error(err)
    error.httpStatusCode = 500;
    return next(error);
  })     
};

//delete Product from cart of the user 
exports.postDeleteProductCart = (req, res, next) => {
  const prodId = req.body.productId;
  Product.findById(prodId)
         .then(product=>{
         return req.user.deleteItemFromCart(product)
         })
  .then(result=>{ 
  
    res.redirect('/cart')
    }
  )
  .catch(err=>{
    const error = new Error(err)
    error.httpStatusCode = 500;
    return next(error);
  })
  
  
}

exports.getInvoice = (req, res, next) => {
  const orderId = req.params.orderId;
  Order.findById(orderId)
       .then(order=>{
        if(!order){
          return next(new Error("No order found."));
        }
        if(order.user.userId.toString()!==req.user._id.toString()){
          return next(new Error('Unauthorized!'))
        }
        const invoiceName = 'invoice-' + orderId + '.pdf';
        const invoicePath = path.join('data','invoices',invoiceName);
        const pdfDoc = new PDFdocument();
        res.setHeader('Content-Type','application/pdf');
        res.setHeader('Content-Disposition','inline; filename="invoice.pdf"');
       
        pdfDoc.pipe(fs.createWriteStream(invoicePath));
        pdfDoc.pipe(res);

        pdfDoc.fontSize(26).text('Invoice',{
          underline:true
        });
        pdfDoc.text('-------------------------');
        order.products.forEach(prod=>{
          pdfDoc.fontSize(20).text(prod.product.title + ' - ' + prod.quantity + ' x ' +prod.product.price+ '€');
        })
        pdfDoc.fontSize(26).text('-------------------------');
        pdfDoc.fontSize(20).text('Total price : '+order.totalPrice+'€');
      
        pdfDoc.end();
      //⛔ bad practice : read data in memory  ⛔//
      //   fs.readFile(invoicePath,(err,data)=>{
      //   if(err){
      //     return next(err);
      //   }
      //   res.setHeader('Content-Type','application/pdf');
      //   res.setHeader('Content-Disposition','inline; filename="invoice.pdf"')   //allows us to define how this content should be served to the client
      //   res.send(data);
      //  })


      // const file = fs.createReadStream(invoicePath); //node will able to use that to read in the file step by step 
     
      // file.pipe(res) //forward the data to the response | response object is a writable stream actually 
  })
  .catch(err=> next(err));
};



