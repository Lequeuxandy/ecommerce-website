const Product = require('../models/Product');
const { validationResult } = require('express-validator/check');
const mongoose = require('mongoose');
const fileHelper = require('../util/file');


//display view admin/edit-product 
exports.getAddProduct = (req, res, next)=>{
    res.render('admin/edit-product',{
        siteTitle:'Add-product',
        path:'/admin/add-product',
        editing:false,
        hasError:false,
        errorMessage: null,
        validationError:[]
        }); 
 };

 //add Product in DB with mangoose
 exports.postAddProduct = (req, res, next)=>{
     const title = req.body.title;
     const image = req.file;
     console.log(image)
     const description = req.body.description;
     const price = req.body.price;
     if(!image){
      return res.status(422)
      .render('admin/edit-product',{
         product:{
           title,
           price,
           description,
         },
         siteTitle:'Add Product',
         path:'/admin/add-product',
         editing:false,
         hasError: true,
         errorMessage: 'Attache file is not an image',
         validationError:[]
     }); 
     }

     const errors = validationResult(req);
     if(!errors.isEmpty()){
     return res.status(422)
         .render('admin/edit-product',{
            product:{
              title,
              price,
              description
            },
            siteTitle:'Add Product',
            path:'/admin/add-product',
            editing:false,
            hasError: true,
            errorMessage: errors.array()[0].msg,
            validationError:errors.array()
        }); 
     }

     const imageUrl = image.path;

     const product = new Product({
       //_id:mongoose.Types.ObjectId('5c7801e8b986ed35404c7aee'),
       title,
       price,
       description,
       imageUrl, 
       userId:req.user //thanks to ref, mangoose will automatically store id in userId 
      }
     );
     //save method is coming from mongoose
     product
        .save()
        .then(result=>{
            console.log('Create Product');
            res.redirect('/admin/products');
     })
     .catch(err=>{
        const error = new Error(err)
        error.httpStatusCode = 500;
        return next(error);
     })
  };

  // display view admin/edit-product with the product to edit
  exports.getEditProduct = (req, res, next)=>{
    const editMode = req.query.edit;
    if(!editMode){
        return res.redirect('/');
    }
    //get product to edit thanks to productID 
    const productId = req.params.productId
    const errors = validationResult(req);
    Product.findById(productId)
    .then(product=>{
        if(!product){
            return res.redirect('/');
        }
        res.render('admin/edit-product',{
            product,
            siteTitle:'Edit-product',
            path:'/admin/edit-product',
            editing:true,
            hasError:false,
            errorMessage: null,
            validationError:[]
            }); 
    })
    .catch(err=>{
      const error = new Error(err)
      error.httpStatusCode = 500;
      return next(error);
    });
};

 //update product 
 exports.postEditProduct = (req,res,next)=>{
     const productId = req.body.productId;
     const updatedtitle = req.body.title;
     const updatedimage = req.file;
     const updateddescription = req.body.description;
     const updatedprice = req.body.price;
     const errors = validationResult(req);
     if(!errors.isEmpty()){
        return res.status(422)
         .render('admin/edit-product',{
            product:{
              title:updatedtitle,
              price:updatedprice,
              description:updateddescription,
              _id:productId
            },
            errorMessage:errors.array()[0].msg,
            siteTitle:'Edit Product',
            path:'/admin/edit-product',
            editing:true,
            hasError: true,
            validationError: errors.array()
        }); 
     }
  
     Product.findById(productId)
            .then(product=>{
              //authorization edit products only for the user who created them
              if(product.userId.toString() !== req.user._id.toString()){
                return res.redirect('/');
              }
              product.title = updatedtitle;
              product.price = updatedprice;
              product.description = updateddescription;
              if(updatedimage){
                fileHelper.deleteFile(product.imageUrl);
                product.imageUrl = updatedimage.path;
              }
             return product.save() //save -> mongoose method | do not crush the product automatically
                          .then(result => {
                            console.log('Product Updated')
                            res.redirect('/admin/products')
                          })
            })
            
            .catch(err=>{
              const error = new Error(err)
              error.httpStatusCode = 500;
              return next(error);
            })
     };

  // display view admin/products with the product list    
  exports.getProducts = (req, res, next) => {
    Product.find({userId:req.user._id})
    // .select('title price description imageUrl -_id')  //🔥🔥 Allows you to choose the properties to use | ➖ does not select properties specify after -
    // .populate('userId','name') //🔥🔥 Allows you to retrieve information from another collection (User) thanks to the id and the references specified in the model | 'name' means that we only want this property
    .then(products=>{
      res.render('admin/products',
      {
        prods:products,
       siteTitle:'admin/products',
        path:'/admin/products'
      }); 
    })
    .catch(err=> {
      const error = new Error(err)
      error.httpStatusCode = 500;
      return next(error);
    });  
   };

   //delete Product
  exports.deleteProduct = (req,res,next)=> {
    const productId = req.params.productId;
    Product.findById(productId)
           .then(product=>{
              if(!product){
                  return next(new Error('product not found'))
              }
              fileHelper.deleteFile(product.imageUrl);
              // Product.findByIdAndRemove(productId)  //method from mangoose
              return Product.deleteOne({_id: productId, userId:req.user._id}) 
           })
           .then(() => {
             console.log('Destroyed product')
            res.status(200).json({message: 'Success!'});
           })
          .catch(err=>{
            res.status(500).json({messsage:'Deleting product failed'});
           })
  };
 
