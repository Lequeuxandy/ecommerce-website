//render 404 error page
exports.getError404 = (req,res,next)=>{
    res.status(404).render('404',{
        siteTitle:'ERROR',
        path:'/404',
        isAuthenticated:req.session.isLoggedin
    });
};

exports.getError500 = (req,res,next)=>{
    res.status(500).render('500',{
        siteTitle:'ERROR',
        path:'/500',
        isAuthenticated:req.session.isLoggedin
    });
};