const express = require('express'),
router = express.Router();
const authController = require('../controllers/auth');
const { check, body } = require('express-validator/check'); 
const User = require('../models/User');


router.get('/login',authController.getLogin) 

router.post('/login',
[
    body('email')
    .isEmail()
    .withMessage('Please enter a valid email')
    .normalizeEmail(),
    body('password',
    //message error for all password validator 
    'Please enter a valid password'
    )
    .isLength({min:5})
    .isAlphanumeric()
    .trim()
],
authController.postLogin)

router.post('/logout',authController.postLogout)

router.get('/signup',authController.getSignup)



//check take the name in form input views (email) and apply verification (isEmail()) and stock errors
router.post('/signup', 
                [
                    check('email')
                    .isEmail()
                    .withMessage('Please enter a valid email')
                    // Custom validator| if we return a promise then express validator will wait for this promise to be fulfilled 
                    .custom((value,{req})=>{
                       return User.findOne({email:value})
                            .then(userDoc=>{
                                if(userDoc){
                                    //throw an error inside of the promise wich will be store in errors validator 
                                    return Promise.reject('E-mail exists already, please pick a different one.')
                                }
                            })
                    })
                    .normalizeEmail(),
                    //check password inside body request 
                    body('password',
                    //message error for all password validator 
                    'Please enter a password with only numbers and text and at least 5 characters'
                    )
                    .isLength({min:5})
                    .isAlphanumeric()
                    .trim(),
                    body('confirmPassword').custom((value,{req}) =>{
                        if(value !== req.body.password){
                            throw new Error('Password have to match!')
                        }
                        return true;
                    })
                    .trim()
                ],
                authController.postSignup)

router.get('/reset',authController.getReset)

router.post('/reset',authController.postReset)

router.get('/reset/:token',authController.getNewPassword)

router.post('/new-password', authController.postNewPassword)


module.exports = router;