const express = require('express'),
adminController = require('../controllers/admin'),
isAuth = require('../middleware/is-auth');
const { body } = require('express-validator/check'); 

const router = express.Router();


//request will be parsed from left to right 

//admin/add-product => GET
router.get('/add-product', isAuth, adminController.getAddProduct) 

//admin/add-product => POST
router.post('/add-product',[
    body('title')
        .isString()
        .isLength({ min:3 })
        .trim(),
    body('price')
        .isFloat(),
    body('description')
        .isLength({min:5,max:200})
        .trim()
],
isAuth, adminController.postAddProduct)

//admin/products => GET
router.get('/products', isAuth, adminController.getProducts) //❗ without parentheses at getProducts because we want to past a reference on the function ❗

//admin/edit-product => GET
router.get('/edit-product/:productId', isAuth, adminController.getEditProduct)

//admin/edit-product => POST
router.post('/edit-product',[
    body('title')
        .isString()
        .isLength({ min:3 })
        .trim(),
    body('price')
        .isFloat(),
    body('description')
        .isLength({min:5,max:200})
        .trim()
], isAuth, adminController.postEditProduct)

//admin/delete-product => POST
router.delete('/product/:productId', isAuth, adminController.deleteProduct)

module.exports = router;



